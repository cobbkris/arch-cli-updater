#!/usr/bin/env bash
# Script by Kris Cobb
# Script for updating Arch system
# Supports AUR, Flatpak, and Snaps

echo "Running Updater"

# Set Variables
ts="N"
pac="N"
aur="N"
fp="N"
snap="N"
tssnapshot=""
pacupdate=""
aurupdate=""
fpupdate=""
snapupdate=""
CHECK_MARK="\033[0;32m\xE2\x9C\x94\033[0m"

# Check for Timeshift via Pacman & Create snapshot
echo "Do you want to create a Timeshift snapshot? [y/N]: "
read ts
if [[ $ts == "y" ]]; then
  echo "Checking for Timeshift installation"
  if pacman -Q timeshift &> /dev/null; then
    echo "Creating Timeshift snapshot"
    sleep 2
    sudo timeshift --create --comments "arch-cli-updater"
    tssnapshot="${CHECK_MARK} Timeshift snapshot created"
  else
    echo "Please check your Timeshift installation"
    sleep 2
  fi
fi  

# Pacman Update
echo "Do you want to update the base system with Pacman? [y/N]: "
read pac
if [[ $pac == "y" ]]; then
  echo "Pacman Updater"
  sleep 2
  sudo pacman -Syu
  pacupdate="${CHECK_MARK} Base System Updated"
fi

# AUR Update
echo "Do you want to update the AUR? [y/N]: "
read aur
if [[ $aur == "y" ]]; then
  echo "AUR Update with Yay"
  sleep 2
  yay -Syu
  aurupdate="${CHECK_MARK} AUR Updated"
fi

# Flatpak Update
echo "Do you want to update Flatpaks? [y/N]: "
read fp
if [[ $fp == "y" ]]; then
  echo "Flatpak Update"
  sleep 2
  flatpak update
  fpupdate="${CHECK_MARK} Flatpaks Updated"
fi

# Snap Update
echo "Do you want to update Snaps? [y/N]: "
read snap
if [[ $snap == "y" ]]; then
  echo "Snap Update"
  sleep 2
  snap refresh
  snapupdate="${CHECK_MARK} Snaps Updated"
fi

# Finishing up
echo "Done"
if [[ $tssnapshot != "" ]]; then
  echo -e $tssnapshot
fi

if [[ $pacupdate != "" ]]; then
  echo -e $pacupdate
fi

if [[ @aurupdate != "" ]]; then
  echo -e $aurupdate
fi

if [[ $fpupdate != "" ]]; then
  echo -e $fpupdate
fi

if [[ $snapupdate != "" ]]; then
  echo -e $snapupdate
fi

sleep 2

exit
