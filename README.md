# Arch CLI Updater

Simple Bash script to update Arch and derivatives Base, AUR, Flatpaks, and Snaps.

Added capability to do a Timeshift snapshot.  This will also check if Timeshift is installed.